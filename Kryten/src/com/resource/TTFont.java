package com.resource;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.TrueTypeFont;

/**
 * Represents group of fonts from same family with different sizes.
 * @author Jakub Chatrný
 *
 */
public class TTFont {
	private Map<Integer,TrueTypeFont> specificFont;
	/**
	 * Creates empty font group
	 */
	public TTFont(){
		specificFont = new HashMap<Integer,TrueTypeFont>();
	}
	/**
	 * Puts new font.
	 * @param key
	 * @param ttf
	 */
	public void put(int key, TrueTypeFont ttf){
		specificFont.put(key, ttf);
	}
	/**
	 * Gets font with specific size. 
	 * @param key
	 * @return
	 */
	public TrueTypeFont get(int key){
		return specificFont.get(key);
	}
		
}
