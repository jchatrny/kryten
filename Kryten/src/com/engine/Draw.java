package com.engine;

import static org.lwjgl.opengl.GL11.*;

import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

/**
 * Draws geometric object to frame buffer.
 * 
 * @author jchatrny
 *
 */
public class Draw {
	/**
	 * Draws rectangle with color and texture. Draw from x, y coordinates.
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param color
	 * @param texture
	 */
	public static void rect(float x, float y, float width, float height,
			Color color, Texture texture) {
		glPushMatrix();
		{
			glTranslatef(x, y, 0);
			glColor4f(color.r, color.g, color.b, color.a);

			texture.bind();

			glBegin(GL_QUADS);
			{
				glTexCoord2f(0, 0);
				glVertex2f(0, 0);
				glTexCoord2f(0, 1);
				glVertex2f(0, height);
				glTexCoord2f(1, 1);
				glVertex2f(width, height);
				glTexCoord2f(1, 0);
				glVertex2f(width, 0);
			}
			glEnd();
		}
		glPopMatrix();
	}

	/**
	 * Draws rectangle with color,texture and rotate it. Draw object from
	 * center.
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param rot
	 * @param color
	 * @param texture
	 */
	public static void rectRot(float x, float y, float width, float height,
			float rot, Color color, Texture texture) {

		glPushMatrix();
		{
			width /= 2;
			height /= 2;
			glTranslatef(x + width, y + height, 0);
			glRotatef(rot, 0, 0, 1);

			glColor4ub((byte) color.getRedByte(), (byte) color.getGreenByte(),
					(byte) color.getBlueByte(), (byte) color.getAlphaByte());
			texture.bind();

			glBegin(GL_QUADS);
			{
				glTexCoord2f(0, 0);
				glVertex2f(-width, -height);
				glTexCoord2f(0, 1);
				glVertex2f(-width, height);
				glTexCoord2f(1, 1);
				glVertex2f(width, height);
				glTexCoord2f(1, 0);
				glVertex2f(width, -height);
			}
			glEnd();
		}
		glPopMatrix();
	}
	/**
	 * Draws rectangle with color and texture. Draw from x, y coordinates.
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param color
	 * @param texture
	 */
	public static void rectRepTex(int x, int y, int width, int height,
			Color color, Texture texture, int scaleX, int scaleY) {
		rectRepTex((float) x, (float)y, (float)width, (float)height, color, texture, (float)scaleX, (float)scaleY);
	}
	public static void rectRepTex(float x, float y, float width, float height,
			Color color, Texture texture, float scaleX, float scaleY) {
		glPushMatrix();
		{
			glTranslatef(x, y, 0);

			glColor4ub((byte) color.getRedByte(), (byte) color.getGreenByte(),
					(byte) color.getBlueByte(), (byte) color.getAlphaByte());
			texture.bind();

			glBegin(GL_QUADS);
			{
				glTexCoord2f(0, 0);
				glVertex2f(0, 0);
				glTexCoord2f(0, scaleY);
				glVertex2f(0, height);
				glTexCoord2f(scaleX, scaleY);
				glVertex2f(width, height);
				glTexCoord2f(scaleX, 0);
				glVertex2f(width, 0);
			}
			glEnd();
		}
		glPopMatrix();
	}
}
